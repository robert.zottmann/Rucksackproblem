Das Rucksackproblem
-------------------
Einreichung im Rahmen der Code Competition Mai 2017.  
https://www.it-talents.de/foerderung/code-competition/code-competition-05-2017  
  
Quellcode: src/  
Ausführbares jar-File: build/dist/  
Javadoc: doc/  
Dokumentation: Dokumentation.pdf  
  
----------  
Ergebnis:  
https://www.it-talents.de/blog/it-talents/die-gewinner-der-code-competition-rucksackproblem  
https://www.it-talents.de/blog/it-talents/der-dritte-platz-bei-der-code-competition-rucksackproblem
package knapsack;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import knapsack.strategies.*;

/**
 * Represents the Knapsack,
 * which is described with a capacity, the current total weight
 * and profit, a list of items (see class Items) and a
 * strategy (see package knapsack.strategies) for creating a solution.
 * @author Robert
 */
public class Knapsack {
	
	// Knapsack Attributes
	private int capacity;
	private int currenKnapsacktWeight; // total weight
	private int currentKnapsackProfit; // total profit
	private Strategy strategy; // current selected strategy
	private ObservableList<Item> itemList = FXCollections.observableArrayList(); // list of all available items
	
	
	/**
	 * Default Constructor.
	 * Initializes a knapsack with chosen default values.
	 */
	public Knapsack(){
	    this.currentKnapsackProfit = 0;
	    this.capacity = 15;
	    this.strategy = new Greedy();
	    assert(invariant()): "inv-ks: capacity, weight or profit is below 0";
	}
	
	/**
	 * Overloaded Constructor.
	 * Initializes a knapsack with default values and chosen parameters.
	 * @param strategy the strategy for creating a solution.
	 * @param capacity the capacity of the knapsack.
	 */
	public Knapsack(Strategy strategy, int capacity){
	    this.currenKnapsacktWeight = 0;
	    this.currentKnapsackProfit = 0;
	    this.capacity = capacity;
	    this.strategy = strategy;
	    assert(invariant()): "inv-ks: capacity, weight or profit is below 0";
	}
	
	/**
	 * Get the current value of the capacity property.
	 * The capacity specifies the amount of space/capacity the knapsack has in total.
	 * @return the current knapsack capacity.
	 */
	public int getCapacity(){
	    return this.capacity;
	}
	
	/**
	 * Get the current value of the currenKnapsacktWeight property.
	 * The weight should be greater than zero.
	 * The knapsack weight specifies the total weight of all items which are currently contained in the knapsack.
	 * @return the total weight of all contained items.
	 */
	public int getCurrentWeight(){
	    return this.currenKnapsacktWeight;
	}
	
	/**
	 * Get the current value of the currentKnapsackProfit property.
	 * The Knapsack profit specifies the total profit of all items which are currently contained in the knapsack.
	 * @return the total profit of all contained items.
	 */
	public int getCurrentProfit(){
	    return this.currentKnapsackProfit;
	}
	
	/**
	 * Get the current value of the strategy property.
	 * The strategy specifies which algorithm is used to create a solution.
	 * @return the current strategy of knapsack.
	 */
	public Strategy getStrategy(){
	    return this.strategy;
	}
	
	/**
	 * Sets the value of the capacity property.
	 * The capacity specifies the amount of space/capacity the knapsack has in total.
	 * @param capacity the capacity which the knapsack should have.
	 */
	public void setCapacity(int capacity){
	    assert(invariant()): "inv-ks: capacity, weight or profit is below 0";
	    this.capacity = capacity;
	    assert(invariant()): "inv-ks: capacity, weight or profit is below 0";
	}
	
	/**
	 * Adds a value to the current knapsack weight.
	 * The weight should be greater than zero.
	 * The knapsack weight specifies the total profit of all items which are currently contained in the knapsack.
	 * @param weight weight which should be added to the current total knapsack profit.
	 */
	private void addToCurrentkWeight(int weight){
	    assert(invariant()): "inv-ks: capacity, weight or profit is below 0";
	    assert(weight >= 0): "Knapsack.addToCurrentkWeight: weight is below zero";
	    this.currenKnapsacktWeight += weight;
	    assert(invariant()): "inv-ks: capacity, weight or profit is below 0";
	}
	
	/**
	 * Adds a value to the current knapsack profit.
	 * The profit should be greater than zero.
	 * The knapsack profit specifies the total profit of all items which are currently contained in the knapsack.
	 * @param profit profit which should be added to the current total knapsack profit.
	 */
	private void addToCurrentProfit(int profit){
	    assert(invariant()): "inv-ks: capacity, weight or profit is below 0";
	    assert(profit >= 0): "Knapsack.addToCurrentProfit: profit is below zero";
	    this.currentKnapsackProfit += profit;
	    assert(invariant()): "inv-ks: capacity, weight or profit is below 0";
	}
	
	/**
	 * Sets the value of the strategy property.
	 * The strategy specifies which algorithm is used to create a solution.
	 * @param strategy the strategy/algorithm we want to use.
	 */
	public void setStrategy(Strategy strategy){
	    this.strategy = strategy;
	}
	
	/**
	 * Adds an item to the knapsack, if there is still enough capacity.
	 * If successful, knapsack weight and profit will be adapted.
	 * @param item the item which we want to try to add to the solution.
	 * @return True, if the item could be added. Otherwise return false.
	 */
	public boolean addItem(Item item){
	    if(capacity-this.getCurrentWeight() >= item.getWeight()){
	        item.packIn(true);
	        this.addToCurrentkWeight(item.getWeight());
	        this.addToCurrentProfit(item.getProfit());
	        return true;
	    }
	    return false;
	}
	
	/**
	 * Adds a list of item to the knapsack.
	 * See {@link Knapsack#addItem(Item)}.
	 * @param items list of items which we want to add.
	 */
        public void addItems(ObservableList<Item> items){
	    items.forEach(e -> this.addItem(e));
	}
	
	/**
	 * Get the current list of all available items for the knapsack to work with.
	 * @return list of all available items.
	 */
        public ObservableList<Item> getItemsList(){
            return this.itemList;
	}
	
	/**
	 * Get the current list of items which are packed in.
	 * This list of items represents a solution and is depending on the strategy.
	 * @return list of items representing a solution.
	 */
	public ObservableList<Item> getPackedInItems(){
	    ObservableList<Item> output = FXCollections.observableArrayList();
		
	    this.getItemsList().forEach( item -> {
	        if(item.isPackedIn())
	            output.add(item);
	    });
	    return output;
	}
	
	/**
	 * Trying to create a solution with the current selected strategy.
	 */
	public void solve(){
	    this.resetKnapsack();
	    this.strategy.algorithm(this);
	}
	
	/**
	 * Reset the knapsack.
	 * The set of items will be emptied and the current weight and profit will be set to zero.
	 */
	public void resetKnapsack(){
	    this.currenKnapsacktWeight = 0;
	    this.currentKnapsackProfit = 0;
	    this.getItemsList().forEach(e -> e.packIn(false));
	    assert(invariant()): "inv-ks: capacity, weight or profit is below 0";
	}

	/*
	 * Invariant Check.
	 * At all times, the capacity, weight and profit of a knapsack should be greater or equal than zero.
	 */
	private boolean invariant(){
	    return (this.getCapacity() >= 0 &&
	            this.getCurrentWeight() >= 0 &&
	            this.getCurrentProfit() >= 0);
	}
}
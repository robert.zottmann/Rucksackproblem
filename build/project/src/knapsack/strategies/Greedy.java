package knapsack.strategies;

import java.util.Collections;

import knapsack.Item;
import knapsack.Knapsack;


/**
 * Represents the Greedy Strategy.
 * The greedy strategy is a heuristic which add
 * items with high profit/weight ratio continuously
 * until capacity is reached.
 * Does not result in an optimal solution.
 * @author Robert
 */
public class Greedy extends TemplateStrategy{
	
	/**
	 * see {@link TemplateStrategy#preprocess(Knapsack)}.
	 * Post-Process Items: Here, filter out all non-valid items.
	 */
	@Override
	protected void preprocess(Knapsack ks){
		for(int i=0; i<ks.getItemsList().size(); i++){
			// item valid: weight>=0 && profit>0
			if(!ks.getItemsList().get(i).isValidItem()){
				ks.getItemsList().remove(i);
				i--;
			}
		}
	}

	/**
	 * see {@link TemplateStrategy#core_algorithm(Knapsack)}.
	 * Core Algorithm: Here, implement a Greedy Heuristic.
	 * Sort (desc) items by their profit/weight ratio.
	 * Pack in items with highest ratio, if there is still capacity.
	 */
	@Override
	protected void core_algorithm(Knapsack ks) {
		// Sort items via Comparator
		Collections.sort(ks.getItemsList(), new Item());
		// add items (packIn)
		ks.addItems(ks.getItemsList());
	}

	/**
	 * see {@link TemplateStrategy#postprocess(Knapsack)}.
	 * Post-Process Items: Here, do nothing.
	 */
	@Override
	protected void postprocess(Knapsack ks) {
		return;	
	}
}
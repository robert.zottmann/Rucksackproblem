package knapsack.strategies;

import knapsack.Knapsack;

/**
 * Strategy Interface.
 * Offering an interface to switch algorithms during runtime.
 * (Strategy Design Pattern).
 * @author Robert
 */
public interface Strategy {
	
	/**
	 * Realizes the algorithm for calculating
	 * a knapsack solution.
	 * @param ks the knapsack which we want to create a solution for.
	 */
	public void algorithm(Knapsack ks);
}
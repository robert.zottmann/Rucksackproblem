package knapsack;

import java.util.Comparator;

/**
 * Represents an Item used for Knapsack.
 * An item is described with a weight and profit.
 * @author Robert
 */
public class Item implements Comparator<Item>{
	
	// Item Attributes
    private int itemWeight;
    private int itemProfit;
    private double itemPWRatio;
    private boolean isPackedIn;
	
	
	/**
	 * Default Constructor.
	 * Initializes an item with chosen default values.
	 */
	public Item(){
	    this.itemProfit = 0;
	    this.itemPWRatio = 0.0;
	    this.isPackedIn = false;
	    assert(invariant()): "cs inv-item: weight or profit is below 0";
	}
	
	/**
	 * Overloaded Constructor.
	 * Initializes an item with chosen default values and the given weight and profit.
	 * @param weight the weight of the item
	 * @param profit the profit of the item
	 */
	public Item(int weight, int profit){
		// check if given weight and profit are valid
		if (!(weight > 0 && profit >= 0)){
			return;
		}
		this.itemWeight = weight;
	    this.itemProfit = profit;
	    this.itemPWRatio = (double)(this.itemProfit)/this.itemWeight;
	    this.isPackedIn = false;
	    assert(invariant()): "cs_ov inv-item: weight or profit is below 0";
	}
	
	/**
	 * Get the value of the itemWeight property.
	 * The itemWeight specifies the weight of an item.
	 * @return the weight of the item.
	 */
    public int getWeight(){
	    return this.itemWeight;
	}
	
	/**
	 * Get the value of the itemProfit property.
	 * The itemProfit specifies the profit of an item.
	 * @return the profit of the item.
	 */
	public int getProfit(){
	    return this.itemProfit;
	}
	
	/**
	 * Get the value of the itemPWRatio property.
	 * The itemPWRatio specifies the ratio Profit/Weight of an item.
	 * @return the ratio Profit/Weight of of the item.
	 */
	public double getPWRatio(){
	    return this.itemPWRatio;
	}
	
	/**
	 * Get the value of the isPackedIn property.
	 * The isPackedIn property specifies if an item is used in the current solution,
	 * which means it is packed in.
	 * @return whether or not if the item is packed in.
	 */
	public boolean isPackedIn(){
	    return this.isPackedIn;
	}
	
	/**
	 * Set the value of the isPackedIn property.
	 * The isPackedIn property specifies if an item is used in the current solution,
	 * which means it is packed in.
	 * @param bvalue whether or not if the item should be packed in.
	 */
	public void packIn(boolean bvalue){
	    this.isPackedIn=bvalue;
	}
	
	/**
	 * Prints the information about an item.
	 * The information is defined in {@link Item#getInfo()}
	 */
	public void printInfo(){
	    System.out.printf(this.getInfo());
	}
	
	/**
	 * Specifies the printed information about an item.
	 * The weight and profit will be printed as a String.
	 * Is used by {@link Item#printInfo()}.
	 * @return String with information about weight and profit of an item
	 */
	public String getInfo(){
	    String info = "Item: Gewicht("+this.getWeight()+"), Profit("+this.getProfit()+")\n";
	    return info;
	}
	
	/**
	 * Check if an item is valid or not.
	 * A valid item should have a positive weight, greater than 0
	 * and a profit greater or equals zero.
	 * @return True, if the item is valid. Otherwise, false
	 */
	public boolean isValidItem(){
	    if (!(this.getWeight() > 0 && this.getProfit() >= 0))
	        return false;
	    return true;
		
	}
	
	/**
	 * Override from Comparator.
	 * Compare items by their itemPWRatio property.
	 * An item with higher profit/weight ratio is 'better'.
	 */
	@Override
	public int compare(Item item1, Item item2) {
	    return Double.compare(item2.getPWRatio(), item1.getPWRatio());
	}
	
	/*
	 * Invariant Check.
	 * At all times, the weight and profit of an item should be greater (or equal) than zero.
	 */
	private boolean invariant(){
	    return (this.getWeight() > 0 &&
	            this.getProfit() >= 0);
	}
}
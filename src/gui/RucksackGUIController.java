package gui;

import java.io.*;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;
import javafx.fxml.FXML;
import javafx.stage.*;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import knapsack.Item;
import knapsack.Knapsack;
import knapsack.strategies.*;

/**
 *  Controller Class for 'RucksackGUI.xml'.
 *  Implements all control elements for the GUI,
 *  which is defined in 'RucksackGUI.fxml'.
 */
public class RucksackGUIController {

/* ------------------
 * ---GUI Elements---
 * ------------------
 */
	
// Buttons
    @FXML // fx:id="b_setCapacity"
    private Button b_setCapacity; 
	
    @FXML // fx:id="b_solve"
    private Button b_solve; 
	
    @FXML // fx:id="b_addItem"
    private Button b_addItem; 
	
// TextFields
    @FXML // fx:id="tf_itemWeight"
    private TextField tf_itemWeight; 
    
    @FXML // fx:id="tf_itemProfit"
    private TextField tf_itemProfit; 
    
    @FXML // fx:id="tf_capacity"
    private TextField tf_capacity; 
    
// Labels
    @FXML // fx:id="lb_endWeight"
    private Label lb_endWeight; 
    
    @FXML // fx:id="lb_endProfit"
    private Label lb_endProfit; 
    
// ChoiceBox
    @FXML // fx:id="cb_selectStrategy"
    private ChoiceBox<Strat> cb_selectStrategy;

// CheckBox
    @FXML // fx:id="cb_time"
    private CheckBox cb_time;
    
// TableViews
    @FXML // fx:id="tv_itemList"
    private TableView<Item> tv_itemList; 
    
    @FXML // fx:id="tv_knapsackItems"
    private TableView<Item> tv_knapsackItems; 
    
// TableColumns
    @FXML // fx:id="col_itemWeight"
    private TableColumn<Item, Integer> col_itemWeight; 
    
    @FXML // fx:id="col_itemProfit"
    private TableColumn<Item, Integer> col_itemProfit; 
    
    @FXML // fx:id="col_knapsackWeight"
    private TableColumn<Item, Integer> col_knapsackWeight; 
    
    @FXML // fx:id="col_knapsackProfit"
    private TableColumn<Item, Integer> col_knapsackProfit; 

// MenuItems    
    @FXML // fx:id="menu_clearAll"
    private MenuItem menu_clearAll; 
    
    @FXML // fx:id="menu_import"
    private MenuItem menu_import; 
    
    @FXML // fx:id="menu_printSolution"
    private MenuItem menu_printSolution; 

    @FXML // fx:id="menu_sampleData"
    private MenuItem menu_sampleData; 
    
    @FXML // fx:id="menu_generateData"
    private MenuItem menu_generateData; 
    
    @FXML // fx:id="menu_badGreedy"
    private MenuItem menu_badGreedy; 
    
    @FXML // fx:id="menu_removeSelectedItem"
    private MenuItem menu_removeSelectedItem; 

/* ----------------------
 * ---Non-GUI Elements---
 * ----------------------
 */
    private Knapsack ks; // knapsack object
    
    private enum Strat {GreedyHeuristik, DynamicProgramming}; // strategies
    
    private boolean printDebugInfo = true; // prints debug information on console
        
/* -------------------------
 * ---GUI Control Methods---
 * -------------------------
 */
    
// -----Initialize GUI-----
    /**
     * Initializing GUI elements.
     */
    @FXML
    void initialize() {
        // Knapsack
    	ks = new Knapsack(new Greedy(), 15);
    	tf_capacity.setText(String.valueOf(ks.getCapacity()));
    	
        // Strategies
    	cb_selectStrategy.getItems().add(Strat.GreedyHeuristik);
    	cb_selectStrategy.getItems().add(Strat.DynamicProgramming);
    	cb_selectStrategy.setValue(Strat.GreedyHeuristik);
    	
        // Initialize columns of itemlist
    	col_itemWeight.setCellValueFactory(new PropertyValueFactory<Item,Integer>("weight"));
    	col_itemProfit.setCellValueFactory(new PropertyValueFactory<Item,Integer>("profit"));
    	col_itemWeight.setStyle("-fx-alignment: CENTER;");
    	col_itemProfit.setStyle("-fx-alignment: CENTER;");
    	
        //tv_itemList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    	
        // Initialize columns of knapsacklist
    	col_knapsackWeight.setCellValueFactory(new PropertyValueFactory<Item,Integer>("weight"));
    	col_knapsackProfit.setCellValueFactory(new PropertyValueFactory<Item,Integer>("profit"));  	
    	col_knapsackWeight.setStyle("-fx-alignment: CENTER;");
    	col_knapsackProfit.setStyle("-fx-alignment: CENTER;");
    	
        // Disable menu elements
    	menu_printSolution.setDisable(true);
    	menu_clearAll.setDisable(true);
    	menu_removeSelectedItem.setDisable(true);
    	cb_time.setSelected(false);   	    	
    }

// -----Menu-----  
    
    // Import Data
    /**
     * Represents the Menu entry for importing data via a csv-file.
     * @param event ActionEvent
     */
    @FXML
    void importData(ActionEvent event) {
    	// Open file chooser dialog
    	FileChooser fileChooser = new FileChooser();
    	fileChooser.setTitle("Waehle csv-Datei zum importieren aus");
    	File csvFile = fileChooser.showOpenDialog(b_addItem.getScene().getWindow());
    	
    	// Check if a file was chosen 
    	if(csvFile == null)
    	    return;
    	
    	//Check file extension
    	String file = csvFile.getName();
    	String fileExt = file.substring(file.lastIndexOf('.')+1);
    	if(!fileExt.equals("csv") && !fileExt.equals("txt")){
    	    printInfoAlert("Ungueltige Datei. Waehlen Sie bitte nur csv-Dateien aus.");
    	    return;
    	}
    	
    	// Import valid data from csv-file
        String line = "";
        String cvsSplitBy = ",";

        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            
            clearAll(event);
        	
            while ((line = br.readLine()) != null) {
                
                // Use cvsSplitBy as separator
                String[] items = line.split(cvsSplitBy);
                
                // Check if at least two values were read in line
                if(items.length < 2)
                	continue;
                
                /* Add items to itemList: first column as weight, second as profit.
                 * Also, check if line/items is/are valid and print Information otherwise */
                addItem(items[0],items[1]);
                
                if(printDebugInfo){System.out.println("Importiere: "+items[0]+cvsSplitBy+items[1]);}
            }

        } catch (IOException e) {
            e.printStackTrace(); // should be non-reachable
        }
    	
        // update TableView
        tv_itemList.setItems(ks.getItemsList());
        
        // Handle menu elements
        if(!ks.getItemsList().isEmpty()){
            menu_clearAll.setDisable(false);
            menu_removeSelectedItem.setDisable(false);
        }
        
    	if(printDebugInfo){System.out.println("Importiere");}   	
    }
    
    // Print Solution Data
    /**
     * Represents the menu entry for solution output.
     * @param event ActionEvent
     */
    @FXML
    void printSolution(ActionEvent event) {
    	// Build String/solution which we want to export
    	StringBuilder stringBuilder = new StringBuilder();
    	
    	String knapsackInfo = "Rucksackproblem\n\n"
    				+ "Kapazitaet: "+ ks.getCapacity()+"\n"
    				+ "Gesamtgewicht: "+ ks.getCurrentWeight()+"\n"
    				+ "Gesamtprofit: " +ks.getCurrentProfit()+"\n"
    				+ "Strategie: "+ cb_selectStrategy.getValue()+"\n"
    				+ "Anzahl Items: "+ ks.getItemsList().size()+"\n";
    	stringBuilder.append(knapsackInfo);
    						
    	stringBuilder.append("\nEingepackte Items ("+ks.getPackedInItems().size()+"):\n");
    	ks.getPackedInItems().forEach( e -> stringBuilder.append(e.getInfo()));
    	
    	stringBuilder.append("\nVerfuegbare Items:\n");
    	ks.getItemsList().forEach( e -> stringBuilder.append(e.getInfo()));
    	
    	
    	// Build Dialog with String/solution 
    	Alert expAalert = new Alert(AlertType.INFORMATION);
    	expAalert.setTitle("Detaliertes Ergebnis");
    	expAalert.setHeaderText("Detaliertes Ergebnis:\n");
    	ButtonType copyClipboard = new ButtonType("In Zwischenablage kopieren");
    	expAalert.getButtonTypes().add(copyClipboard);
    	
    	// Create content with solution
    	TextArea solution = new TextArea(stringBuilder.toString());
    	solution.setEditable(false);
    	solution.setWrapText(true);

    	solution.setMaxWidth(Double.MAX_VALUE);
    	solution.setMaxHeight(Double.MAX_VALUE);
    	GridPane.setVgrow(solution, Priority.ALWAYS);
    	GridPane.setHgrow(solution, Priority.ALWAYS);

    	// Create pane for expandable content
    	GridPane expContent = new GridPane();
    	expContent.setMaxWidth(Double.MAX_VALUE);
    	expContent.add(solution, 0, 0);

    	// Set expandable content into the dialog pane
    	expAalert.getDialogPane().setExpandableContent(expContent);
    	expAalert.getDialogPane().setExpanded(true);	

    	// Check which Button was pressed
    	Optional<ButtonType> result = expAalert.showAndWait();
    	
    	// if Button copy to clipboard was pressed
    	if (result.get() == copyClipboard){
    	    Clipboard clipboard = Clipboard.getSystemClipboard();
            ClipboardContent content = new ClipboardContent();
            
            content.putString(stringBuilder.toString());
            clipboard.setContent(content);
            //printSolution(event); // to stay in dialog (not pretty)
    	}
    	//else: OK Button
    	
    	if(printDebugInfo){System.out.println("Exportiere");}
    }

    // Clear item list (table on the left side)
    /**
     * Represents the menu entry for clearing the whole knapsack.
     * @param event ActionEvent
     */
    @FXML
    void clearAll(ActionEvent event) {
        // Clear both lists and set knapsack attributes to zero
    	ks.getItemsList().clear();
    	tv_itemList.setItems(ks.getItemsList());
    	tv_knapsackItems.setItems(ks.getPackedInItems());
    	lb_endWeight.setText("0");
    	lb_endProfit.setText("0");
		
        // Handle menu elements
    	menu_clearAll.setDisable(true);
    	menu_removeSelectedItem.setDisable(true);
    	menu_printSolution.setDisable(true);
    	
    	if(printDebugInfo){System.out.println("Leere Itemliste");}
    }
    
    // Remove single item (left table)
    /**
     * Represents the menu entry for deleting a single available item.
     * @param event ActionEvent
     */
    @FXML
    void removeSelectedItem(ActionEvent event) {
    	// Get selected Item and remove it
        Item i = tv_itemList.getSelectionModel().getSelectedItem();
        ks.getItemsList().remove(i);
        
	// Reset Knapsack, since current solution may be not valid anymore
        ks.resetKnapsack();
        tv_knapsackItems.setItems(ks.getPackedInItems());
        lb_endWeight.setText(String.valueOf(ks.getCurrentWeight()));
        lb_endProfit.setText(String.valueOf(ks.getCurrentProfit()));
        menu_printSolution.setDisable(true);
        
        // Handle menu elements
        if(ks.getItemsList().isEmpty()){
            menu_clearAll.setDisable(true);
            menu_removeSelectedItem.setDisable(true);
	}     
	
	if(printDebugInfo){System.out.print("Item wird entfernt: "); i.printInfo();}		
    }
    
    // Show an example, that Greedy is not optimal
    /**
     * Represents the menu entry for showing an example that Greedy is not optimal.
     * @param event ActionEvent
     */
    @FXML
    void badGreedy(ActionEvent event) {
    	clearAll(event);
    	ks.getItemsList().add(new Item(1,2));
    	ks.getItemsList().add(new Item(3,3));
    	
    	tv_itemList.setItems(ks.getItemsList());
    	
    	menu_clearAll.setDisable(false);
    	menu_removeSelectedItem.setDisable(false);
    	
    	ks.setCapacity(3);
    	tf_capacity.setText(String.valueOf(ks.getCapacity()));
    	
    	ks.setStrategy(new Greedy());
    	cb_selectStrategy.setValue(Strat.GreedyHeuristik);
    	b_solve(event);
    	
    	if(printDebugInfo){System.out.println("Beispiel Greedy");}
    }

    // Add items as an example
    /**
     * Represents the menu entry for adding example data.
     * @param event ActionEvent
     */
    @FXML
    void sampleData(ActionEvent event) {
    	clearAll(event);
    	ks.getItemsList().add(new Item(1,8));
    	ks.getItemsList().add(new Item(2,6));
    	ks.getItemsList().add(new Item(4,10));
    	ks.getItemsList().add(new Item(6,12));
    	ks.getItemsList().add(new Item(2,1));
    	ks.getItemsList().add(new Item(3,1));
    	
    	tv_itemList.setItems(ks.getItemsList());
    	
    	menu_clearAll.setDisable(false);
    	menu_removeSelectedItem.setDisable(false);
    	
    	ks.setCapacity(12);
    	tf_capacity.setText(String.valueOf(ks.getCapacity()));
    	
    	if(printDebugInfo){System.out.println("Beispieldaten");}
    }
    
    // Generate n random items
    /**
     * Represents the menu entry for generating sample data out of user input.
     * @param event ActionEvent
     */
    @FXML
    void generateData(ActionEvent event) {    	
    	// Get input
    	TextInputDialog dialog = new TextInputDialog("10");
    	dialog.setTitle("Anzahl zufallsgeneriterter Items");
    	dialog.setHeaderText(null);
    	dialog.setContentText("Wie viele zufallsgenerierte Items möchten Sie erstellen:");
    	Optional<String> result = dialog.showAndWait();
    	
    	// If input valid, try to generate random items
    	if (result.isPresent()){
    	    String number = result.get();
    	    
    	    if(isValid(number)){
    	    	clearAll(event);
    	    	menu_clearAll.setDisable(false);
        	menu_removeSelectedItem.setDisable(false);
        	
    	    	try{
    	    	    for(int i=0; i<Integer.valueOf(number); i++){
    	    		int weight = ThreadLocalRandom.current().nextInt(1, ks.getCapacity()+2);
	    	    	int profit = ThreadLocalRandom.current().nextInt(0, (ks.getCapacity()+1)*3);
	    	    	ks.getItemsList().add(new Item(weight,profit));
	    	    }
    		} catch(OutOfMemoryError e){
    		    printInfoAlert("Zu wenig Speicher zum erstellen der Items :(");
    		    clearAll(event);
    	    	}	
    	    }
    	} 
    	else // no valid input
    	    return;
    	
    	// update TableView
    	tv_itemList.setItems(ks.getItemsList());
    	
    	if(printDebugInfo){System.out.println(result.get()+" Zufallsitems");}
    }
    
// -----Buttons-----
 
    // Set knapsack capacity
    /**
     * Represents the button for setting the knapsack capacity.
     * @param event ActionEvent
     */
    @FXML
    void b_setCapacity(ActionEvent event) {
    	// Get input
    	String capacity = tf_capacity.getText();
    	// Input valid?
    	if(!isValid(capacity)){
    	    tf_capacity.setText(String.valueOf(ks.getCapacity()));
    	    return;
    	}
    	
    	// Set capacity
    	ks.setCapacity(Integer.parseInt(capacity));

    	if(printDebugInfo){System.out.println("Kapazitaet: " + ks.getCapacity());}
    }

    // Add defined item
    /**
     * Represents the button for adding a defined item via textfields.
     * @param event ActionEvent
     */
    @FXML
    void b_addItem(ActionEvent event) {
        // Get input
    	String weightInput = tf_itemWeight.getText();
    	String profitInput = tf_itemProfit.getText();
		    	
        // Check and Add input
    	if(!addItem(weightInput, profitInput))
    	    return;
    	tv_itemList.setItems(ks.getItemsList());
    	
    	menu_clearAll.setDisable(false);
    	menu_removeSelectedItem.setDisable(false);
    	    	
    	tf_itemWeight.clear();
    	tf_itemProfit.clear();
    	
    	if(printDebugInfo){System.out.println("Neues Item: Gewicht("+weightInput+"), Profit("+profitInput+")");}
    }

    // Create a solution with current strategy
    /**
     * Represents the button for creating a solution under the current strategy.
     * @param event ActionEvent
     */
    @FXML
    void b_solve(ActionEvent event) {	    	
    	// Set selected strategy 
    	Strat strategy = cb_selectStrategy.getValue();
    	switch(strategy){
    	    case GreedyHeuristik:
    	        if(printDebugInfo){System.out.println("Greedy Strategie");}
    	        ks.setStrategy(new Greedy());
    	        break;
    	    case DynamicProgramming:
    	        if(printDebugInfo){System.out.println("DynamicProgramming Strategie");}
    	        ks.setStrategy(new DynamicProgramming());
    	        break;
    	}
    	
        // Try to create a solution
    	long start = 0; 
    	long end = 0;
    	try{
            start = System.currentTimeMillis();
            ks.solve();
            end = System.currentTimeMillis();
    	}catch(OutOfMemoryError e){
            printInfoAlert("Zu wenig Speicher zur Berechnung :(");
            ks.resetKnapsack();
            tv_knapsackItems.setItems(ks.getPackedInItems());
            lb_endWeight.setText(String.valueOf(ks.getCurrentWeight()));
            lb_endProfit.setText(String.valueOf(ks.getCurrentProfit()));
            menu_printSolution.setDisable(true);
            return;
    	}
    	
    	tv_knapsackItems.setItems(ks.getPackedInItems());
		
        // Set solution
        lb_endWeight.setText(String.valueOf(ks.getCurrentWeight()));
        lb_endProfit.setText(String.valueOf(ks.getCurrentProfit()));
		
        // Handle menu items
        if(ks.getPackedInItems().isEmpty()){
            ks.resetKnapsack();
            printInfoAlert("Keine Lösung gefunden.");
            }
        else{
            menu_printSolution.setDisable(false);
			
            if(cb_time.isSelected())
                printTimeInfo(start,end);
            }
			
        if(printDebugInfo){System.out.println("Neue Loesung mit Gewicht "+ks.getCurrentWeight()+" und Profit "+ks.getCurrentProfit());}
    }
    
/* ---------------------
 * ---Utility methods---
 * ---------------------
 */
    
    //Try to add an item defined by user input to the list of valid, available items.
    private boolean addItem(String weight, String profit){
    	
    	// leave if input is not valid
        if(!isValid(weight) || !isValid(profit)){
            tf_itemWeight.clear();
            tf_itemProfit.clear();
            return false;
    	}
    	
    	// Add input
    	ks.getItemsList().add(new Item(Integer.parseInt(weight), Integer.parseInt(profit)));
    	return true;
    }
    
    
    // Check is input is valid -> positive Integer
    private boolean isValid(String input){
    	// is Integer?
    	if(!isInt(input))
    	    return false;
    	
    	// is positive?
    	if(Integer.parseInt(input) < 0){
    	    if(printDebugInfo){System.out.println("Wert negativ!");}
            printInfoAlert("Ganzzahlige Werte muessen positiv sein.");
            return false;
    	}
    	return true;
    }
    
    // Check if input is an Integer
    private boolean isInt(String input){
    	try{
    	    Integer.parseInt(input);
    	    return true;
    	}catch(NumberFormatException e){
    	    if(printDebugInfo){System.out.println("Keine Zahl!");}
    	    printInfoAlert("Geben Sie bitte nur ganzzahlige Werte ein.");
    	    return false;
    	}
    }
    
    // Show an Information-Alert dialog window
    private void printInfoAlert(String message){
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Information.");
        alert.setHeaderText(null);
        alert.setContentText(message);

        alert.showAndWait();
    }
	
    // Show runtime in milliseconds
    private void printTimeInfo(long start, long end){
        long result = end-start;
        printInfoAlert("Zeit: "+String.valueOf(result)+" Millisekunde(n).");
    }	  
}
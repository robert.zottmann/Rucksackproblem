package gui;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.fxml.FXMLLoader;

/**
 * Main class of the Knapsack program.
 * We will use JavaFX as GUI Framework together with the 
 * package 'knapsack' as implementation.
 * @author Robert
 */
public class Main extends Application {
	
    /**
	 * JavaFX.
	 * Create and define JavaFX GUI/Stage here.
	 */
	@Override
	public void start(Stage primaryStage) {
	    try {
	        AnchorPane root = (AnchorPane)FXMLLoader.load(getClass().getResource("RucksackGUI.fxml"));
	        Scene scene = new Scene(root,1000,800);
	        //scene.getStylesheets().add(getClass().getResource("RucksackGUI.css").toExternalForm()); // no css used
	        primaryStage.setTitle("Rucksackproblem");
	        //primaryStage.setResizable(false); // fix window
	        primaryStage.setScene(scene);
	        primaryStage.show();
	        }
        catch(Exception e) {
            e.printStackTrace();
        }
	}
	
	public static void main(String[] args) {
	    // launch GUI
	    launch(args);
	}
}
package knapsack.strategies;

import knapsack.Knapsack;

/**
 * Abstract Class TemplateStrategy.
 * Defines a template on how an algorithm should look like
 * (Template Design Pattern).
 * @author Robert
 */
public abstract class TemplateStrategy implements Strategy {
	
	/**
	 * Perform a pre-processing of all available items for knapsack.
	 * @param ks the knapsack we want to work with
	 */
	protected abstract void preprocess(Knapsack ks);
	
	/**
	 * Apply the pure algorithm to decide which item we want to pack in the knapsack.
	 * @param ks the knapsack we want to work with
	 */
	protected abstract void core_algorithm(Knapsack ks);
	
	/**
	 * Perform a post-processing for e.g. backtracking, cleaning; if necessary.
	 * @param ks the knapsack we want to work with
	 */
	protected abstract void postprocess(Knapsack ks);
	
	/**
	 * Apply the complete algorithm.
	 * This consists of three parts, represented by the three abstract methods:
	 * 1. performing a pre-processing
	 * 2. apply the pure algorithm
	 * 3. perform a post-processing, if necessary
	 * (Any new strategy will be described with this template). 
	 */
	@Override
	public void algorithm(Knapsack ks){
		// 1.preprocess
		preprocess(ks);
		// 2. core_algorithm 
		core_algorithm(ks);
		// 3. postprocess
		postprocess(ks);
		
	}

}
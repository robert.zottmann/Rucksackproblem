package knapsack.strategies;

import knapsack.Knapsack;

/**
 * Represents the DynamicProgramming Strategy.
 * Create an optimal solution out of optimal sub-solutions. 
 * @author Robert
 */
public class DynamicProgramming extends TemplateStrategy{
	
	// DynamicProgramming Attributes
	private boolean printTableau = false; // enable to print tableau for debugging
	private int dpTable[][]; // represents tableau

	/**
	 * see {@link TemplateStrategy#preprocess(Knapsack)}.
	 * Post-Process Items: Here, filter out all non-valid items.
	 */
	@Override
	protected void preprocess(Knapsack ks) {
		// Reuse Greedy code here, which does also the filtering
		Greedy g = new Greedy();
		g.preprocess(ks);
	}

	/**
	 * see {@link TemplateStrategy#core_algorithm(Knapsack)}.
	 * Core Algorithm: Dynamic Programming (0-1-Knapsack).
	 * See documentation.
	 */
	@Override
	protected void core_algorithm(Knapsack ks) {
		
		int numberItems = ks.getItemsList().size();
		int knapsackCapacity = ks.getCapacity();

		dpTable = new int[numberItems+1][knapsackCapacity+1];
		
		/*
		 *  Algorithm for Dynamic Programming.
		 *  Loop over 2-dimensional array and do a case analysis.
		 */
		for(int i=0; i<=numberItems; i++){
			
			for(int j=0; j<=knapsackCapacity; j++){
				/*
				 * First Case:
				 * Initialize first row and column with 0s. Seems like a waste of space at first,
				 * but is actually needed for the computation of the next row(s).
				 */
				if(i==0 || j == 0){
					dpTable[i][j] = 0;
				}
				/*
				 * Second Case:
				 * If weight of item is below current capacity (column),
				 * get the maximum of: 1. the item value in the previous row
				 *                 OR  2. value of current item + value of item in previous row with 'minus current item weight'-column
				 * -> choosing if it's better to use current item at current capacity (j) or not
				 */
				else if(ks.getItemsList().get(i-1).getWeight() <= j){
					dpTable[i][j] = maxInt(dpTable[i-1][j],
										   ks.getItemsList().get(i-1).getProfit() + dpTable[i-1][j - ks.getItemsList().get(i-1).getWeight()]);
				}
				/*
				 * Third Case:
				 * Get the value 'above' (table-wise), meaning the value in the previous row, instead.
				 */
				else{
					dpTable[i][j] = dpTable[i-1][j];
				}
				
				if(printTableau){System.out.print(dpTable[i][j] + " ");}
				
			}//end j-loop
			
			if(printTableau){System.out.println();}
			
		}// end i-loop
		
	}
	
	// Utility method, returning max value of two Integers
	private int maxInt (int item1, int item2){
		return (item1 > item2) ? item1 : item2;
	}

	/**
	 * see {@link TemplateStrategy#postprocess(Knapsack)}.
	 * Post-Process Items: backtrack table to determine which item is packed in specifically.
	 * Returns only ONE valid (and optimal) item allocation.
	 */
	@Override
	protected void postprocess(Knapsack ks) {
		
		if(dpTable == null)
			return;

		int numberItems = ks.getItemsList().size();
		int j = ks.getCapacity();
		
		/*
		 * Backtrack over solution/table.
		 * Start bottom right corner: if value in current row != previous row -> packIn item and distract item weight,
		 * otherwise (value does not differ) item is not added to Knapsack.
		 */
		for(int i=numberItems; i>0; i--){
				if(dpTable[i][j] != dpTable[i-1][j]){
					ks.addItem(ks.getItemsList().get(i-1));
					j -= ks.getItemsList().get(i-1).getWeight();
				}
		}
		
	}
}